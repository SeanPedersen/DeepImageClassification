""" Serves Keras models to train on CIFAR-10 """

__author__ = "Sean Pedersen"

import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D, Input, ZeroPadding2D
from keras.layers.merge import concatenate
from keras.applications.vgg19 import VGG19
from keras.applications.vgg19 import preprocess_input, decode_predictions
from keras.layers.advanced_activations import LeakyReLU, PReLU, ELU
from keras.layers.normalization import BatchNormalization

class Models():

    def __init__(self):
        self.acti_n = 0

    # Returns activation function with unique name
    def _get_acti(self, activation, alpha=0.3):
        # Add number to activation name to prevent equivalent names which cause exceptions
        acti_name = activation+str(self.acti_n)
        if activation == "leakyrelu": activation = LeakyReLU(alpha=alpha, name=acti_name)
        elif activation == "elu": activation = ELU(name=acti_name)
        elif activation == "prelu": activation = PReLU(name=acti_name)
        else: activation = Activation(activation, name=acti_name)
        self.acti_n += 1
        return activation

    # Transfer learning VGG19
    # So far not really impressive... maybe use the first dense layer as output instead of last conv
    def vgg19(self, optimizer, dropout, input_shape, output_shape):
        # Load pre-trained convolutional layer part
        # include_top=True requires (224, 224, 3) input shape (rescaling eats up RAM)
        vgg19_conv_model = VGG19(weights="imagenet", include_top=False, input_shape=input_shape)
        # Freeze pretrained layers (exclude them from training)
        for l in vgg19_conv_model.layers:
            l.trainable = False

        model = Sequential(name="VGG19@CIFAR-10")
        model.add(vgg19_conv_model)
        model.add(Flatten())
        # Add last dense layer for the classification
        model.add(Dense(1024, activation="tanh"))
        model.add(Dense(128, activation="sigmoid"))
        if dropout: model.add(Dropout(dropout))
        model.add(Dense(output_shape, activation="softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    # Simple dense (fully-connected) neural network
    def dense_NN(self, optimizer, dropout, activation, input_shape, output_shape):
        # Define model graph (architecture / topology)
        model = Sequential(name="dense_NN")

        # now: model.output_shape == (32, 32, 3)
        # Since no convolution, we do not keep track of 2 dimensionality, so we flatten
        # the tensor to a vector
        model.add(Flatten(input_shape=input_shape, name="Flatten"))
        # now: model.output_shape == (3072,)
        # Dense (fully connected) layers
        model.add(Dense(256, name="Dense-256"))
        model.add(Activation(activation))
        if dropout: model.add(Dropout(dropout))
        model.add(Dense(128)) # best eval: 0.9210
        model.add(Activation("relu"))
        if dropout: model.add(Dropout(dropout))

        """model.add(Dense(16)) # 0.9175
        model.add(Activation("relu"))
        if dropout: model.add(Dropout(0.5))"""

        # Dense output layer with "num_classes" neurons and softmax activation function
        # softmax: The output sum of all neurons is 1.0 (statistical scale)
        model.add(Dense(output_shape, name="Dense-"+str(output_shape)))
        model.add(Activation("softmax", name="Softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    # Straight convolutional neural network
    # @ epoch 10: acc: 81% val_acc: 76% ReLu
    # val_acc 77% @ leakyrelu
    # val_acc 80% @ LeaykRelu + augmentation
    # @ epoch 46: acc: 77% val_acc: 83% LeaykRelu + augmentation
    def straight_CNN(self, optimizer, dropout, activation, input_shape, output_shape):
        self.acti_n = 0
        # Define model graph (architecture / topology)
        model = Sequential(name="straight_CNN")

        kernel_size = (3, 3)

        # Convolutional 2D-Layers (https://keras.io/layers/convolutional/#conv2d)
        # Paramerts:
        # "filters": Number of weights attached to the convolving window
        # "kernel_size": Defines the 2D window which convolves across the whole 2D input
        model.add(Conv2D(48, kernel_size=kernel_size, padding="same", input_shape=input_shape))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(96, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.20))

        model.add(Conv2D(192, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.20))

        # Since no convolution, we do not keep track of 2 dimensionality, so we flatten
        # the tensor to a vector
        model.add(Flatten())
        model.add(Dense(256))
        model.add(self._get_acti(activation))
        model.add(Dropout(0.35))

        # Dense output layer with "num_classes" neurons and softmax activation function
        # softmax: The output sum of all neurons is 1.0 (statistical scale)
        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    def straight_CNN_small(self, optimizer, dropout, activation, input_shape, output_shape):
        self.acti_n = 0
        model = Sequential(name="straight_CNN_small")

        kernel_size = (3, 3)

        model.add(Conv2D(32, kernel_size=kernel_size, padding="same", input_shape=input_shape))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(64, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        if dropout: model.add(Dropout(dropout))

        model.add(Flatten())
        model.add(Dense(128))
        model.add(self._get_acti(activation))
        if dropout: model.add(Dropout(dropout))

        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    def CNN_small_drop23(self, optimizer, dropout, activation, input_shape, output_shape):
        self.acti_n = 0
        model = Sequential(name="CNN_small_drop23")

        kernel_size = (3, 3)

        model.add(Conv2D(32, kernel_size=kernel_size, padding="same", input_shape=input_shape))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(64, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))

        model.add(Flatten())
        model.add(Dense(128))
        model.add(self._get_acti(activation))
        model.add(Dropout(0.4))

        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    def CNN_small_drop3(self, optimizer, dropout, activation, input_shape, output_shape):
        self.acti_n = 0
        model = Sequential(name="CNN_small_drop3")

        kernel_size = (3, 3)

        model.add(Conv2D(32, kernel_size=kernel_size, padding="same", input_shape=input_shape))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(64, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())
        model.add(Dense(128))
        model.add(self._get_acti(activation))
        model.add(Dropout(0.4))

        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    def CNN_small_drop_full(self, optimizer, dropout, activation, input_shape, output_shape):
        self.acti_n = 0
        model = Sequential(name="CNN_small_drop_full")

        kernel_size = (3, 3)

        model.add(Conv2D(32, kernel_size=kernel_size, padding="same", input_shape=input_shape))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.4))

        model.add(Conv2D(64, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.4))

        model.add(Flatten())
        model.add(Dense(128))
        model.add(self._get_acti(activation))
        model.add(Dropout(0.4))

        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    def straight_CNN_med(self, optimizer, dropout, activation, input_shape, output_shape):
        self.acti_n = 0
        model = Sequential(name="straight_CNN_med")

        kernel_size = (3, 3)

        model.add(Conv2D(48, kernel_size=kernel_size, padding="same", input_shape=input_shape))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(96, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(192, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())
        model.add(Dense(256))
        model.add(self._get_acti(activation))

        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    def straight_CNN_big(self, optimizer, dropout, activation, input_shape, output_shape):
        self.acti_n = 0
        model = Sequential(name="straight_CNN_big")

        kernel_size = (3, 3)

        model.add(Conv2D(64, kernel_size=kernel_size, padding="same", input_shape=input_shape))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(128, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(256, kernel_size=kernel_size, padding="same"))
        model.add(self._get_acti(activation))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())
        model.add(Dense(512))
        model.add(self._get_acti(activation))
        model.add(Dense(256))
        model.add(self._get_acti(activation))

        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    # Strides > 1 to reduce dimensionality (try parallel strides for 1. layer)
    def stride_CNN_small(self, optimizer, dropout, activation, input_shape, output_shape):

        # Define model graph (architecture / topology)
        model = Sequential(name="stride_CNN_small")

        kernel_size = (3, 3)
        stride_size = (2, 2)

        model.add(Conv2D(32, kernel_size=kernel_size, strides=stride_size, padding="same", input_shape=input_shape))
        model.add(self._get_acti(activation))

        model.add(Conv2D(64, kernel_size=kernel_size, strides=stride_size, padding="same"))
        model.add(self._get_acti(activation))

        model.add(Flatten())
        model.add(Dense(128))
        model.add(self._get_acti(activation))

        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        model.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=["accuracy"])
        return model

    # Parallel dense neural network
    def parallel_DNN(self, optimizer, dropout, input_shape, output_shape):
        para_dense = 4 # Number of parallel dense layers

        # Create seperate model graph for parallel dense layers
        inp = Input(shape=input_shape)
        dense_outputs = []
        for _ in range(para_dense):
            flat = Flatten(input_shape=input_shape)(inp)
            dense = Dense(128, activation)(flat)
            dense_outputs.append(dense)

        # Concatenate the output tensors of the dense layers
        out = concatenate(dense_outputs)
        # Define the parallel dense layers model
        parallel_dense = Model(input=inp, output=out)

        model = Sequential()
        model.add(parallel_dense)
        model.add(Dense(128))
        model.add(Activation("relu"))
        model.add(Dense(128))
        model.add(Activation("relu"))

        # Dense output layer with "num_classes" neurons and softmax activation function
        # softmax: The output sum of all neurons is 1.0 (statistical scale)
        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    # Parallel convolutional neural network
    # Parallel ConvLayers are unneccessary... since we got multiple filters (neurons)
    # NOTES: Tanh yields promising results
    def parallel_CNN(self, optimizer, dropout, activation, input_shape, output_shape):
        filters = 48
        para_convs = 3 # Number of parallel convolutional layers

        # Create seperate model graph for parallel convolutions
        inp = Input(shape=input_shape)
        conv_outputs = []
        for i in range(para_convs):
            conv = Conv2D(filters=filters, kernel_size=(3, 3), activation=activation, input_shape=input_shape)(inp)
            pool = MaxPooling2D(pool_size=(2,2))(conv)
            conv_outputs.append(pool)

        # Concatenate the output tensors of the convolutional layers
        out = concatenate(conv_outputs)
        # Define the parallel conv model
        parallel_convs = Model(input=inp, output=out)

        model = Sequential()
        model.add(parallel_convs) # add parallel layers
        model.add(Conv2D(filters=64, kernel_size=(4, 4), padding="same"))
        model.add(Activation(activation))
        model.add(Conv2D(filters=96, kernel_size=(3, 3)))
        model.add(Activation(activation))
        model.add(MaxPooling2D(pool_size=(2,2)))
        if dropout: model.add(Dropout(0.25))
        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation("relu"))
        if dropout: model.add(Dropout(0.5))
        # model.add(Dense(256))
        # model.add(Activation(activation))
        # if dropout: model.add(Dropout(0.5))

        # Dense output layer with "num_classes" neurons and softmax activation function
        # softmax: The output sum of all neurons is 1.0 (statistical scale)
        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        # Compile the model for training
        model.compile(loss="categorical_crossentropy",
                      optimizer=optimizer,
                      metrics=["accuracy"])
        return model

    # Source: http://parneetk.github.io/blog/cnn-cifar10/
    # @ leakyrelu val_acc: 78.72%
    def parn_CNN(self, optimizer, dropout, activation, alpha, input_shape, output_shape):
        if activation == "leakyrelu": activation = LeakyReLU(alpha=alpha)
        elif activation == "elu": activation = ELU()
        elif activation == "prelu": activation = PReLU()
        else: activation = Activation(activation)
        # Define the model
        model = Sequential()
        model.add(Conv2D(48, kernel_size=(3, 3), padding="same", input_shape=input_shape))
        model.add(activation)
        model.add(Conv2D(48, kernel_size=(3, 3)))
        model.add(activation)
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))
        model.add(Conv2D(96, kernel_size=(3, 3), padding="same"))
        model.add(activation)
        model.add(Conv2D(96, kernel_size=(3, 3)))
        model.add(activation)
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))
        model.add(Conv2D(192, kernel_size=(3, 3), padding="same"))
        model.add(activation)
        model.add(Conv2D(192, kernel_size=(3, 3)))
        model.add(activation)
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))
        model.add(Flatten())
        model.add(Dense(512))
        model.add(activation)
        model.add(Dropout(0.5))
        #model.add(Dense(256))
        #model.add(Activation(activation))
        #model.add(Dropout(0.3))
        model.add(Dense(output_shape, activation="softmax"))
        # Compile the model
        model.compile(optimizer=optimizer, loss="categorical_crossentropy", metrics=["accuracy"])
        return model

    # Source: https://www.bonaccorso.eu/2016/08/06/cifar-10-image-classification-with-keras-convnet/
    def bonna_CNN(self, optimizer, dropout, activation, input_shape, output_shape):
        if activation == "leakyrelu": activation = LeakyReLU(alpha=alpha)
        elif activation == "elu": activation = ELU()
        elif activation == "prelu": activation = PReLU()
        else: activation = Activation(activation)
        # Create Keras model
        model = Sequential()

        model.add(Conv2D(32, 3, 3, border_mode="valid", input_shape=input_shape))
        model.add(activation)
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(64, 3, 3, border_mode="valid"))
        model.add(activation)
        model.add(ZeroPadding2D((1, 1)))

        model.add(Conv2D(128, 3, 3, border_mode="valid"))
        model.add(activation)
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(ZeroPadding2D((1, 1)))

        # Try to remove some convolutional layer
        model.add(Conv2D(256, 3, 3, border_mode="valid"))
        model.add(activation)
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(ZeroPadding2D((1, 1)))

        model.add(Conv2D(512, 3, 3, border_mode="valid"))
        model.add(activation)
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(ZeroPadding2D((1, 1)))

        model.add(Dropout(0.2))
        model.add(Flatten())
        model.add(Dense(128))
        model.add(activation)
        model.add(Dropout(0.2))
        model.add(Dense(output_shape))
        model.add(Activation("softmax"))

        model.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=["accuracy"])
        return model


if __name__ == "__main__":
    print("Only serves models...")
