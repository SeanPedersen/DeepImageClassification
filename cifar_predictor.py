""" Load trained models and predict labels of images """

__author__ = "Sean Pedersen"

import helper
import os
import numpy as np
import keras
from keras.datasets import cifar10
from keras.models import load_model

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' # Disable Tensorflow warnings

model_name = "stride_CNN"
model_hash = "30ec25c49b09f3fdc3571a25e2550e8be8dc1f1d803b673639fff8caca1593bf"

EVAL = 1
two_classes = True

(x_train, y_train), (x_test, y_test) = cifar10.load_data()
num_classes = 10
if two_classes:
    (x_train, y_train), (x_test, y_test) = helper.reduce_two_cats(0, 1, x_train, y_train, x_test, y_test)
    num_classes = 2

# Convert integer classes (0-9) to one-hot vectors
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model_path = "./logs/"+model_name+"/"+model_hash+"/best_model.h5"
model = load_model(model_path)

if EVAL:
    score = model.evaluate(x=x_test, y=y_test, verbose=0)
    print(model.loss)
    print(model.optimizer)
    print("\nval_acc:", score[1])
    print("val_loss:", score[0])
else:
    # Single predicition
    data_sample = np.expand_dims(x_test[0], axis=0)
    y_score = model.predict(data_sample)
    print(y_score)
