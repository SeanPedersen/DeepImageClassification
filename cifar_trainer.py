""" Training environment for the CIFAR-10 training dataset """

__author__ = "Sean Pedersen"

import cifar_models # My defined models
import helper # Helper functions
import os
import logging
import json
from time import time
import numpy as np
import keras
from keras import backend as K
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.utils import plot_model
from keras.callbacks import Callback, TensorBoard, CSVLogger, ModelCheckpoint

class Trainer():

    def __init__(self, config={}):
        # Setup logging
        logging.basicConfig(filename="trainer.log", level=logging.DEBUG)
        stderrLogger = logging.StreamHandler()
        stderrLogger.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
        logging.getLogger().addHandler(stderrLogger)

        # Set random seed for reproducibilty (used for initializing weights & biases)
        np.random.seed(56)
        K.clear_session()

        # Load / create config
        config_name = "params.json"
        if config:
            self.config = config
        elif os.path.exists(config_name):
            # Read in JSON config file
            with open(config_name, "r") as fp:
                self.config = json.load(fp)
        else:
            logging.info("No config.json found... Creating default config.json")
            self.config = {}
            # Console Outputs & Logging
            self.config["logging"] = True # Save training progress & best model
            self.config["show_progress"] = 1 # Show training progress
            self.config["TF_CPP_MIN_LOG_LEVEL"] = "3" # Show tensorflow messages
            # Dataset options
            self.config["two_classes"] = [] # False = [], True = [0-9,0-9]
            self.config["data_augmentation"] = False
            self.config["upscale"] = False
            self.config["greyscale"] = False
            # Backpropagation hyper-parameters
            self.config["batch_size"] = 48
            self.config["epochs"] = 10
            # Optimizer hyper-parameters
            self.config["learning_rate"] = 0.001
            self.config["lr_decay"] = 0.01

            with open(config_name, 'w') as fp:
                json.dump(self.config, fp)
        os.environ["TF_CPP_MIN_LOG_LEVEL"] = self.config["TF_CPP_MIN_LOG_LEVEL"]

    def prep_data(self):
        # CIFAR-10 dataset split into train and test sets:
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()

        if self.config["greyscale"]:
            logging.info("Converting RGB-images to greyscale")
            x_train = helper.rgb2grey(x_train)
            x_test = helper.rgb2grey(x_test)
            logging.info("Greyscaled images")

        if self.config["upscale"]:
            new_shape = (48, 48, 3) # VGG-19 min shape is (48,48,3)
            logging.info("Scaling images up to: "+str(new_shape))
            x_train = helper.upscale_img(x_train, new_shape)
            x_test = helper.upscale_img(x_test, new_shape)
            logging.info("Upscaled images")

        if self.config["two_classes"]:
            logging.info("Reducing Dataset to 2 classes: "+ str(self.config["two_classes"]))
            first_class = self.config["two_classes"][0]
            second_class = self.config["two_classes"][1]
            (x_train, y_train), (x_test, y_test) = helper.reduce_two_cats(first_class, second_class, x_train, y_train, x_test, y_test)
            num_classes = 2
            logging.info("Reduced classes")
        else:
            # Default: use all 10 classes for training
            num_classes = 10

        logging.info("x_train shape: "+str(x_train.shape)+" x_test shape: "+str(x_test.shape))
        logging.info("y_train shape: "+str(y_train.shape)+" y_test shape: "+str(y_test.shape))

        logging.info(str(x_train.shape[0])+" train samples")
        logging.info(str(x_test.shape[0])+" test samples")

        # Convert integer classes (default: 0-9) to one-hot vectors
        y_train = keras.utils.to_categorical(y_train, num_classes)
        y_test = keras.utils.to_categorical(y_test, num_classes)

        # Normalize input data values to a scale of 0.0-1.0
        x_train = x_train.astype("float32")
        x_test = x_test.astype("float32")
        x_train /= 255
        x_test /= 255

        # Expose some vars
        self.num_classes = num_classes
        self.input_shape = x_train.shape[1:]
        self.x_train = x_train
        self.y_train = y_train
        self.x_test = x_test
        self.y_test = y_test

    def start(self, model):
        # Starts training. Is dependent of prep_data/1

        logging.info("MODEL NAME: "+model.name)
        logging.info("Model params: "+str(model.count_params()))
        model.summary() # Print model topology

        # Model Callbacks (triggered at each epoch start&end)

        # Custom callback
        training_cb = helper.TrainingCallback(self.config)
        # Run TensorBoard in working directory: $ tensorboard --logdir Graph/
        # Open web-interface: http://0.0.0.0:6006/
        tensorboard = TensorBoard(log_dir="Graph", histogram_freq=0, write_graph=True, write_images=True)

        if self.config["logging"]:
            #callbacks = [training_cb, tensorboard] # for linux only
            callbacks = [training_cb]
        else:
            callbacks = []

        # Shrink train_data?
        #x_train, y_train = x_train[:10000], y_train[:10000]

        # Training begins
        if not self.config["data_augmentation"]:
            logging.info("Not using data augmentation.")

            history = model.fit(self.x_train, self.y_train,
                      batch_size=self.config["batch_size"],
                      epochs=self.config["epochs"],
                      validation_data=(self.x_test, self.y_test),
                      shuffle=True,
                      verbose=self.config["show_progress"],
                      callbacks=callbacks)
        else:
            logging.info("Using real-time data augmentation.")

            # This will do preprocessing and realtime data augmentation:
            datagen = ImageDataGenerator(
                featurewise_center=False,  # set input mean to 0 over the dataset
                samplewise_center=False,  # set each sample mean to 0
                featurewise_std_normalization=False,  # divide inputs by std of the dataset
                samplewise_std_normalization=False,  # divide each input by its std
                zca_whitening=False,  # apply ZCA whitening
                rotation_range=10,  # randomly rotate images in the range (degrees, 0 to 180)
                width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
                height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
                horizontal_flip=True,  # randomly flip images
                vertical_flip=False)  # randomly flip images

            datagen.fit(self.x_train)

            # Fit the model on the batches generated by datagen.flow().
            history = model.fit_generator(datagen.flow(self.x_train, self.y_train, batch_size=self.config["batch_size"]),
                                             steps_per_epoch=self.x_train.shape[0] // self.config["batch_size"],
                                             epochs=self.config["epochs"],
                                             validation_data=(self.x_test, self.y_test),
                                             verbose=self.config["show_progress"],
                                             callbacks=callbacks)
