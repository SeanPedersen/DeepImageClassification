""" Serves helper functions """

__author__ = "Sean Pedersen"

import numpy as np
import pandas as pd
import scipy
from keras.callbacks import Callback
from time import time
import csv
import json
import os
import logging
import hashlib


class TimingCallback(Callback):
    # Custom callback saving execution time of every epoch in list

    def __init__(self):
        self.times=[]
    def on_epoch_begin(self, epoch, logs):
        self.starttime=time()
    def on_epoch_end(self, epoch, logs):
        self.times.append(time()-self.starttime)

class TrainingCallback(Callback):
    # Custom callback saving history (accs&losses) & epoch time & best model

    def __init__(self, config):
        self.best_val_acc = 0.0
        self.config = config

    def on_train_begin(self, logs={}):
        # using YAML to hash, since JSON is unordered and thus produces different hashes on seperate runs of the same model
        yaml_string = self.model.to_yaml()
        # or you order the json by keys (items)
        config_string = str([value for (key, value) in sorted(self.config.items())]) #TODO: Remove for training irrelevant params (show_progress, TF_CPP_MIN_LOG_LEVEL)
        models_dir_name = "./models/"
        self.model_hash = sha256(yaml_string+config_string)
        model_dir_path = models_dir_name+self.model.name+"/"
        self.model_save_path = model_dir_path+self.model_hash+"/"
        self.csv_path = self.model_save_path+"training_log.csv"
        # check if dirs exist
        if not os.path.exists(models_dir_name): os.mkdir(models_dir_name)
        if not os.path.exists(model_dir_path): os.mkdir(model_dir_path)
        if not os.path.exists(self.model_save_path): os.mkdir(self.model_save_path)

        # Save training params JSON config
        with open(self.model_save_path+"params.json", "w") as fp:
            json.dump(self.config, fp)

        # Save model topology / architecture
        with open(self.model_save_path+"model_topo.yaml", "w") as f:
            f.write(yaml_string)
        #print("json", self.model.to_json())
        #print("HASH", self.model_hash)

        # Delete old csv training log
        if os.path.exists(self.csv_path): os.remove(self.csv_path)

    def on_train_end(self, logs):
        logging.info("Results saved: \n"+self.model.name+"/"+self.model_hash+"\n\n")
        self.plot_data()

    def on_epoch_begin(self, epoch, logs={}):
        self.starttime=time()

    def on_epoch_end(self, epoch, logs={}):
        # Save log (accs, losses, epoch_time)
        epoch_time = time()-self.starttime
        logs["epoch_time"] = epoch_time
        self.log_to_csv(logs)
        # Save best model (all: weights, opti/loss, state, architecture)
        # TODO: consider train_acc as well (weight boths difference etc.)
        if logs["val_acc"] > self.best_val_acc:
            self.model.save(self.model_save_path+"best_model.h5")
            self.best_val_acc = logs["val_acc"]

    def log_to_csv(self, logs_dict):
        if not os.path.exists(self.csv_path):
            with open(self.csv_path, "w", newline="") as csvfile:
                writer = csv.writer(csvfile, delimiter=",")
                writer.writerow(list(logs_dict.keys()))

        with open(self.csv_path, "a", newline="") as csvfile:
            writer = csv.writer(csvfile, delimiter=",")
            writer.writerow(list(logs_dict.values()))

    def plot_data(self):
        # Plot recorded training data
        path = self.csv_path
        training_df = pd.read_csv(path, header=0)

        epoch_times = training_df["epoch_time"].tolist()
        avg_epoch_time = round(sum(epoch_times) / len(epoch_times), 1)

        plot_df = training_df[["acc","val_acc","loss","val_loss"]]
        path = path.replace("\\", "/") # FU Windows...
        plot_title = path.split("/")[2] + " - avg_epoch_time: " + str(avg_epoch_time) + "sec"
        plot = plot_df.plot(title=plot_title)
        plot.set_xlabel("epochs")
        # Save plot to .PNG file
        plot.get_figure().savefig(self.model_save_path+"training_plot.png")

def sha256(string):
    # Calc sha256 hash of a string
    return str(hashlib.sha256(string.encode("utf-8")).hexdigest())

def upscale_img(images, new_shape):
    # upscale input images (np.array) to new_shape (size)
    return np.array([scipy.misc.imresize(image, new_shape) for image in images])

def rgb2grey(images, extra_dim=True):
    # Implements naive average grey scaling
    # convert 3 channel rgb images (x,y,3) to single channel average (x,y,1) images
    if extra_dim:
        return np.array([np.expand_dims(image.astype(float).sum(axis=-1) / 3.0, axis=2) for image in images])
    else: # does not nest single channel in extra array (for matplotlib)
        return np.array([image.astype(float).sum(axis=-1) / 3.0 for image in images])

def reduce_two_cats(cat_1_index, cat_2_index, x_train, y_train, x_test, y_test):
    if cat_1_index >= cat_2_index:
        raise ValueError("first class is >= second class")
    # returns two categories (cat_1_index & cat_2_index) of the original 10 of CIFAR-10
    two_train = [(sample, y_class) for (sample, y_class) in list(zip(x_train, y_train)) if y_class == [cat_1_index] or y_class == [cat_2_index]]
    x_train = np.array([x for (x,_) in two_train])
    y_train = np.array([y//cat_2_index for (_,y) in two_train])
    two_test = [(sample, y_class) for (sample, y_class) in list(zip(x_test, y_test)) if y_class == [cat_1_index] or y_class == [cat_2_index]]
    x_test = np.array([x for (x,_) in two_test])
    y_test = np.array([y//cat_2_index for (_,y) in two_test])
    return (x_train, y_train), (x_test, y_test)
