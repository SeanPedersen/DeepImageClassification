# Schedule training for different models & configurations
import keras
from cifar_trainer import Trainer
from cifar_models import Models

if __name__ == "__main__":
    trainer = Trainer()
    models = Models()
    # Select optimizer for backpropagation: [SGD(), Adam(), Nadam(), rmsprop()]
    #opt = keras.optimizers.rmsprop(lr=learning_rate, decay=lr_decay)
    opt = keras.optimizers.Adam()

    # E10-L2 (CAT&DOG)
    trainer.config["epochs"] = 10
    trainer.config["two_classes"] = [3,5]
    trainer.prep_data()
    trainer.start(models.stride_CNN_small(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
    trainer.start(models.CNN_small_drop23(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
    trainer.start(models.CNN_small_drop3(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
    trainer.start(models.CNN_small_drop_full(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))

    # E10-L2 (DEER&SHIP)
    trainer.config["epochs"] = 10
    trainer.config["two_classes"] = [4,8]
    trainer.prep_data()
    trainer.start(models.stride_CNN_small(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
    trainer.start(models.CNN_small_drop23(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
    trainer.start(models.CNN_small_drop3(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
    trainer.start(models.CNN_small_drop_full(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))

    #E50-L10 (ALL)
    trainer.config["epochs"] = 50
    trainer.config["two_classes"] = []
    trainer.prep_data()
    trainer.start(models.stride_CNN_small(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
    trainer.start(models.CNN_small_drop23(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
    trainer.start(models.CNN_small_drop3(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
    trainer.start(models.CNN_small_drop_full(opt, 0.0, "relu", trainer.input_shape, trainer.num_classes))
