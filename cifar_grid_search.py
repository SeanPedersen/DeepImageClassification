""" Experimentation environment: Using Grid-Search to find good hyper-parameters
"""

import helper
from itertools import product
import functools
import os
import cifar_models # My defined models
import numpy as np
np.random.seed(42)
import tensorflow as tf
import keras
from keras import backend as K
from keras.datasets import cifar10

# load data
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

# Reduce to 2 classes
(x_train, y_train), (x_test, y_test) = helper.reduce_two_cats(3, 5, x_train, y_train, x_test, y_test)
num_classes = 2

# Convert integer classes (0-9 / 0-1) to one-hot vectors
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)
# Normalize input data values to a scale of 0.0-1.0
x_train = x_train.astype("float32")
x_test = x_test.astype("float32")
x_train /= 255
x_test /= 255

# tweak params
tf_log = False
verbose = 3
# hyper-parameters to search through:
adam = keras.optimizers.Adam()
learning_rate = 0.001 # 0.0001
lr_decay = 1e-2 # 1e-6
rmsprop = keras.optimizers.rmsprop(lr=learning_rate, decay=lr_decay)
batch_sizes = [48]
opts = [adam, rmsprop]
activations = ["tanh","relu","leakyrelu"]
grid_params = (batch_sizes, opts, activations)
if not tf_log: os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' # Disable Tensorflow warnings

grid_search_results = []
models = cifar_models.Models()

grid = product(*grid_params)
num_params = [len(param) for param in grid_params]
num_permus = functools.reduce(lambda x, y: x*y, num_params)
print("PERMUTATIONS: ", num_permus)
# Loop over all parameter permutations
for current_params in grid:
    np.random.seed(42)
    tf.set_random_seed(42)
    # split up
    (batch_size, opt, activation) = current_params
    opt_name = str(type(opt)).split("'")[1].split(".")[2]
    if verbose:
        print("batch_size:", batch_size)
        print("optimizer:", opt_name)
        print("activation:", activation)

    model = models.straight_CNN(opt, 0.0, activation, x_train.shape[1:], num_classes)
    timing_cb = helper.TimingCallback()
    history = model.fit(x_train[:1000], y_train[:1000],
              batch_size=batch_size,
              epochs=4,
              validation_data=(x_test, y_test),
              shuffle=True,
              verbose=verbose,
              callbacks=[timing_cb])

    param_names = (batch_size, opt_name, activation)
    #data_rows = list(zip(history.history["val_acc"], timing_cb.times))
    #df = pd.DataFrame.from_records(data_rows, columns=["val_acc", "train_time"])
    #print(df)
    grid_search_results.append([param_names, [history.history["val_acc"], history.history["acc"], timing_cb.times]])

# evaluation of the recorded grid permutations
best_grid_results = {}
for result in grid_search_results:
    grid_permutation, data_rows = result
    val_accs, train_accs, train_times = data_rows
    best_val_acc = max(val_accs)
    val_acc_time_ratio = sum(val_accs)/sum(train_times)
    best_train_acc = max(train_accs)
    train_acc_time_ratio = sum(train_accs)/sum(train_times)
    if verbose == 3:
        print(grid_permutation)
        print(data_rows)
        print("best val_acc:", best_val_acc)
        print("val_accs/train_times:", val_acc_time_ratio)
        print("best train_acc:", best_train_acc)
        print("train_accs/train_times:",train_acc_time_ratio)
    best_grid_results[grid_permutation] = (best_val_acc, val_acc_time_ratio,
                                            best_train_acc, train_acc_time_ratio)

best_grid_val_acc = sorted(best_grid_results.items(), key=lambda x: x[1][0], reverse=True) # find best val_acc
print("Global best val_acc permu: ", best_grid_val_acc[0][0], " @val_acc: ", best_grid_val_acc[0][1][0], "@train_acc: ", best_grid_val_acc[0][1][2])
# TODO: Add fastest converging model (low train time + high val_acc)
