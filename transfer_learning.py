"""
Currently in cifar_models @vgg19
TODO: Training environment for retraining pretrained CNNs (Transfer Learning)
"""

__author__ = "Sean Pedersen"

import cifar_models # My own class for models
import os
import numpy as np
import keras
from keras.applications.vgg19 import VGG19
from keras.applications.vgg19 import preprocess_input, decode_predictions
from keras.models import Model
